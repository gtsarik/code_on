function initNotDelete() {
    $('a#delete-group').click(function(event) {
        alert('Не возможно удалить группу пока есть хоть один студент!');
    });
}

function initDateFields() {
  $('input.dateinput').datetimepicker({
    format: 'YYYY-MM-DD',
    locale: 'uk'
  }).on('dp.hide', function(event){
    $(this).blur();
  });
}

function initCreateModal() {
  $('a#create-form-link').click(function(event){
    var link = $(this);
    $.ajax({
      'url': link.attr('href'),
      'dataType': 'html',
      'type': 'get',
      'success': function(data, status, xhr){
        // check if we got successfull response from the server
        if (status != 'success') {
          alert('Ошибка на сервере. Попробуйте позже.');
          return false;
        }

        // update modal window with arrived content from the server
        var modal = $('#modalCreate'),
          html = $(data), form = html.find('#content-column form');
        modal.find('.modal-title').html(html.find('#content-column h3').text());
        modal.find('.modal-body').html(form);

        // // setup and show modal window finally
        // modal.modal('show');

        // init our edit form
        initCreateModalForm(form, modal);

        // setup and show modal window finally
        modal.modal({
          'keyboard': false,
          'backdrop': false,
          'show': true
        });
      },
      'error': function(){
          alert('Ошибка на сервере. Попробуйте позже.');
          return false
      }
    });

    return false;
  });
}

function initCreateModalForm(form, modal) {
    // attach datepicker
    initDateFields();

    // close modal window on Cancel button click
    form.find('input[name="cancel_button"]').click(function(event){
        modal.modal('hide');
        return false;
    });

    // make form work in AJAX mode
    form.ajaxForm({
        'dataType': 'html',
        'error': function(){
            alert('Ошибка на сервере. Попробуйте позже.');
            return false;
        },

        'beforeSend': function(xhr, settings){
            // indicator.show();
            $("#submit-id-add_button").prop("disabled", true);
            $("#submit-id-cancel_button").prop("disabled", true);
            $('.form-horizontal').html('<img id="loader-img" alt="" src="/static/img/preloader.gif" width="50" height="50" align="center" />', 3000);
        },
        'error': function(xhr, status, error){
            // alert(status);
            // alert('Не возможно удалить группу пока есть хоть один студент!');
            alert(error);
            // indicator.hide();
        },

        'success': function(data, status, xhr) {
            var html = $(data), newform = html.find('#content-column form');

            // copy alert to modal window
            modal.find('.modal-body').html(html.find('.alert'));

            // copy form to modal if we found it in server response
            if (newform.length > 0) {
                modal.find('.modal-body').append(newform);

                // initialize form fields and buttons
                initCreateModalForm(newform, modal);
            } else {
                // if no form, it means success and we need to reload page
                // to get updated students list;
                // reload after 2 seconds, so that user can read success message
                setTimeout(function(){location.reload(true);}, 500);
            }
            $("#submit-id-add_button").prop("disabled", false);
        }
    });
}

$(document).ready(function(){
    initCreateModal();
    initDateFields();
    initNotDelete();
});
