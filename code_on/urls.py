from django.conf.urls import patterns, include, url
from django.contrib import admin

from .settings import MEDIA_ROOT
from app_s.students.views import GroupView, GroupCreateView, StudentView
from app_s.students.views import StudentUpdateView, StudentCreateView
from app_s.students.views import StudentDeleteView
from app_s.students.views import GroupUpdateView, GroupDeleteView


urlpatterns = patterns(
    '',

    # Groupos Url
    url(r'^$', GroupView.as_view(), name='groups'),
    url(r'^groups/add/$', GroupCreateView.as_view(), name='groups_add'),
    url(r'^groups/(?P<pk>\d+)/edit/$', GroupUpdateView.as_view(), name='groups_edit'),
    url(r'^groups/(?P<pk>\d+)/delete/$', GroupDeleteView.as_view(), name='groups_delete'),

    # Students Url
    url(r'^students/(?P<pk>\d+)?/$', StudentView.as_view(), name='students'),
    url(r'^students/add/$', StudentCreateView.as_view(), name='students_add'),
    url(r'^students/(?P<pk>\d+)/edit/$', StudentUpdateView.as_view(), name='students_edit'),
    url(r'^students/(?P<pk>\d+)/delete/$', StudentDeleteView.as_view(), name='students_delete'),

    # Admin Url
    url(r'^admin/', include(admin.site.urls)),
)

urlpatterns += patterns(
    '',
    url(r'^media/(?P<path>.*)$', 'django.views.static.serve',
        {'document_root': MEDIA_ROOT})
)
