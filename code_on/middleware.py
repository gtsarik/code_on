# -*- coding: utf-8 -*-
from django.db import connection
from code_on.settings import DEBUG


class QueryNumberOfSqlMiddleware(object):
    def process_response(self, request, response):
        # If it is not in debug mode,
        # Then immediately return a request
        if not DEBUG:
            return response

        if request.META['CONTENT_TYPE'] == 'text/plain':
            # Determine the number of queries to the database
            db_query_len = len(connection.queries)

            if db_query_len:
                # Loop through the list of dictionaries,
                # we are interested only in time
                db_queries = connection.queries
                db_time = sum([(float(q['time']) * 1000) for q in db_queries])
            else:
                db_time = 0.0

            stats = {
                'Time query set': db_time,
                'The Number of SQL queries': db_query_len,
            }

            # Prepare html-code - to put our data
            start_tag_str = '''
                        <div class="container">
                        <div class="row">
                        <div class="col-xs-12">'''
            end_tag_str = '</div></div></div></body>'
            stat_str = start_tag_str + \
                ';  '.join('{0} = {1:.2f}'.format(k, v)
                    for (k, v) in stats.items()) + \
                end_tag_str

            content = response.content
            # Adding to the document data queries
            response.content = content.replace('</body>', stat_str)

        return response
