# -*- coding: utf-8 -*-
import os
from code_on import settings


def absolutePathUrl(request):
    ''' Returns absolute site root '''

    separ = os.sep
    domen_url_list = request.build_absolute_uri().split(separ)
    domen_url = domen_url_list[0] + separ + separ + domen_url_list[2]

    return {'ABSPATH_URL': domen_url}


def all_settings(request):
    # from django.contrib import admin
    print '==== admin.site.urls ==== ', request.user.id
    return {'SETTINGS': settings}
