# -*- coding: utf-8 -*-
from django.contrib import admin
from django.core.urlresolvers import reverse
from django.forms import ModelForm, ValidationError

from .models import Student, Group


class StudentInline(admin.TabularInline):
    model = Student
    extra = 1


class StudentFormAdmin(ModelForm):
    def clean(self):
        """Check if student is leader in any group.
        If yes, then ensure it's the same as selected group."""

        # get group where current student is a leader
        groups = Group.objects.filter(leader=self.instance)

        if len(groups) > 0 and \
                self.cleaned_data['student_group'] != groups[0]:
            raise ValidationError(
                u'Студент является старостой другой группы.',
                code='invalid')

        return self.cleaned_data['student_group']


class StudentAdmin(admin.ModelAdmin):
    form = StudentFormAdmin
    list_display = ['last_name', 'first_name', 'ticket', 'student_group']
    list_display_links = ['last_name', 'first_name']
    list_editable = ['student_group']
    ordering = ['last_name']
    list_filter = ['student_group']
    list_per_page = 10
    search_fields = [
        'last_name', 'first_name', 'middle_name',
        'ticket', 'notes']

    def view_on_site(self, obj):
        return reverse('students_edit', kwargs={'pk': obj.id})


class GroupAdmin(admin.ModelAdmin):
    inlines = [StudentInline,]
    list_display = ['title', 'get_leader']
    list_display_links = ['title', 'get_leader']
    ordering = ['title']
    list_filter = ['title']
    list_per_page = 10
    search_fields = ['title', 'get_leader']


admin.site.register(Student, StudentAdmin)
admin.site.register(Group, GroupAdmin)
