from .groups import GroupView
from .groups_add import GroupCreateView
from .students import StudentView
from .students_add import StudentCreateView
from .students_edit import StudentUpdateView
from .students_delete import StudentDeleteView
from .groups_edit import GroupUpdateView
from .groups_delete import GroupDeleteView
