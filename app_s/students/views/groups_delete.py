# -*- coding: utf-8 -*-
from django.views.generic import DeleteView
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.views.generic.base import TemplateView

from ..models import Group


class GroupDeleteView(DeleteView):
    model = Group
    template_name = 'groups_delete.html'

    def get_success_url(self):
        return u'%s?status_message=Группа успешно удалена!' \
            % reverse('groups')
