# -*- coding: utf-8 -*-
from django.views.generic import DeleteView
from django.core.urlresolvers import reverse

from ..models import Student


class StudentDeleteView(DeleteView):
    model = Student
    template_name = 'students_delete.html'

    def get_success_url(self):
        return u'%s?status_message=Студент успешно удален!' \
            % reverse('groups')
