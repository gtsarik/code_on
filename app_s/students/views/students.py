# -*- coding: utf-8 -*-
from django.views.generic.base import TemplateView

from ..models import Student, Group
from code_on.util import paginate


class StudentView(TemplateView):
    template_name = 'students.html'

    def get_context_data(self, **kwargs):
        '''View Student Page'''

        context = super(StudentView, self).get_context_data(**kwargs)

        try:
            current_group = Group.objects.get(id=kwargs['pk'])
            students = Student.objects.filter(student_group_id=current_group.id)
            data = paginate(students, 2, self.request, {})
            context['paginate'] = data
            context['current_group'] = current_group.title
        except Exception:
            context['paginate'] = None
            context['current_group'] = None

        return context
