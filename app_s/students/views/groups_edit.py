# -*- coding: utf-8 -*-
from django.forms import ModelForm
from django.views.generic import UpdateView
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit
from crispy_forms.bootstrap import FormActions

from ..models import Group


class GroupUpdateForm(ModelForm):
    class Meta:
        model = Group
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(GroupUpdateForm, self).__init__(*args, **kwargs)

        self.helper = FormHelper(self)

        # set form tag attributes
        self.helper.form_action = reverse('groups_edit',
            kwargs={'pk': kwargs['instance'].id})
        self.helper.form_method = 'POST'
        self.helper.form_class = 'form-horizontal'

        # set form field properties
        self.helper.help_text_inline = True
        self.helper.html5_required = True
        self.helper.label_class = 'col-sm-2 control-label'
        self.helper.field_class = 'col-sm-10'

        # add buttons
        self.helper.layout.fields.append(FormActions(
            Submit('add_button', u'Сохранить', css_class="btn btn-primary"),
            Submit('cancel_button', u'Отмена', css_class="btn btn-link"),
        ))


class GroupUpdateView(UpdateView):
    model = Group
    template_name = 'groups_edit.html'
    form_class = GroupUpdateForm

    def get_success_url(self):
        return u'%s?status_message=Группа успешно сохранена!' \
            % reverse('groups')

    def post(self, request, *args, **kwargs):
        if request.POST.get('cancel_button'):
            return HttpResponseRedirect(
                u'%s?status_message=Редактирование группы отменено!' %
                reverse('groups'))
        else:
            return super(GroupUpdateView, self).post(request, *args, **kwargs)
