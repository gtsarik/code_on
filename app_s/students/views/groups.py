# -*- coding: utf-8 -*-
from django.views.generic.base import TemplateView

from ..models import Group, Student


class GroupView(TemplateView):
    template_name = 'groups.html'

    def get_context_data(self, **kwargs):
        '''View Groups Page'''

        context = super(GroupView, self).get_context_data(**kwargs)

        try:
            groups = []
            groups_all = Group.objects.order_by('title')

            for group in groups_all:
                temp = []
                count_students = Student.objects.filter(
                    student_group_id=group.id).count()
                temp.append(group)
                temp.append(count_students)
                groups.append(temp)

            context['groups'] = groups
        except Exception:
            context['groups'] = None

        return context
