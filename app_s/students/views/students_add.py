# -*- coding: utf-8 -*-
from django.forms import ModelForm
from django.views.generic import CreateView
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit
from crispy_forms.bootstrap import FormActions

from ..models import Student


class StudentCreateForm(ModelForm):
    class Meta:
        model = Student
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(StudentCreateForm, self).__init__(*args, **kwargs)

        self.helper = FormHelper(self)

        # set form tag attributes
        self.helper.form_action = reverse('students_add')
        self.helper.form_method = 'POST'
        self.helper.form_class = 'form-horizontal'

        # set form field properties
        self.helper.help_text_inline = True
        self.helper.html5_required = True
        self.helper.label_class = 'col-sm-2 control-label'
        self.helper.field_class = 'col-sm-10'

        # add buttons
        self.helper.layout.fields.append(FormActions(
            Submit('add_button', u'Сохранить', css_class="btn btn-primary"),
            Submit('cancel_button', u'Отмена', css_class="btn btn-link"),
        ))


class StudentCreateView(CreateView):
    model = Student
    template_name = 'students_add.html'
    form_class = StudentCreateForm

    def get_success_url(self):
        return u'%s?status_message=Студент успешно добавлен!' \
            % reverse('groups')

    def post(self, request, *args, **kwargs):
        if request.POST.get('cancel_button'):
            return HttpResponseRedirect(
                u'%s?status_message=Добавление студента отменено!' %
                reverse('groups'))
        else:
            return super(StudentCreateView, self).post(request, *args, **kwargs)
