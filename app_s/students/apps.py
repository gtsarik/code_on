# -*- coding: utf-8 -*-
from django.apps import AppConfig


class StudentsAppConfig(AppConfig):
    name = 'app_s.students'
    verbose_name = u'База студентов'

    def ready(self):
        from app_s.students import signals
