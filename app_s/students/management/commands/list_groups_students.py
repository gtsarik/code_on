# -*- coding: utf-8 -*-
from django.core.management.base import NoArgsCommand
from optparse import make_option


class Command(NoArgsCommand):
    option_list = NoArgsCommand.option_list + (
        make_option(
            '--list', action='store_true',
            dest='list',
            default=False,
            help='View list groups and students in current group'),
    )
    help = 'Prints view list groups and students in current group.'

    requires_model_validation = True

    def handle_noargs(self, **options):
        from app_s.students.models import Group, Student

        lines = []
        groups = Group.objects.all()

        for group in groups:
            lines.append(u'Группа %s: ' % group.title)
            students = Student.objects.filter(student_group_id=group.id)

            for student in students:
                lines.append('%s %s' % (student.last_name, student.first_name))

            lines.append('\n')

        return "\n".join(lines)
