# -*- coding: utf-8 -*-
# Receive the pre_delete signal and delete the file associated with the model instance.
from django.db.models.signals import pre_delete
from django.dispatch.dispatcher import receiver
from django.db import models


class Student(models.Model):
    """Student Model"""

    class Meta(object):
        unique_together = ('first_name', 'middle_name', 'last_name')
        verbose_name = u"Студент"
        verbose_name_plural = u"Студенты"

    last_name = models.CharField(
        max_length=256,
        blank=False,
        verbose_name=u"Фамилия")

    first_name = models.CharField(
        max_length=256,
        blank=False,
        verbose_name=u"Имя")

    middle_name = models.CharField(
        max_length=256,
        blank=True,
        verbose_name=u"Отчество",
        default='')

    birthday = models.DateField(
        blank=False,
        verbose_name=u"Дата рождения",
        null=True)

    photo = models.ImageField(
        blank=True,
        verbose_name=u"Фото",
        null=True)

    ticket = models.CharField(
        max_length=256,
        blank=False,
        unique=True,
        verbose_name=u"Билет")

    student_group = models.ForeignKey(
        'students.Group',
        verbose_name=u"Группа",
        blank=False,
        null=True,
        on_delete=models.PROTECT)

    def __unicode__(self):
        return u"%s %s" % (self.last_name, self.first_name)

    def pic(self):
        if self.photo:
            return u'<img src="%s" width=70>' % self.photo.url
        else:
            return ('none')
    pic.short_description = u'Фото'
    pic.allow_tags = True


@receiver(pre_delete, sender=Student)
def photo_delete(sender, instance, **kwargs):
    # Pass false so FileField doesn't save the model.
    instance.photo.delete(False)
