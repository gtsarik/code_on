# -*- coding: utf-8 -*-
from django.db import models


class Group(models.Model):
    """Group Model"""

    class Meta(object):
        verbose_name = u"Группу"
        verbose_name_plural = u"Группи"

    title = models.CharField(
        max_length=256,
        blank=False,
        unique=True,
        verbose_name=u"Название")

    leader = models.OneToOneField(
        'Student',
        verbose_name=u"Староста",
        blank=True,
        null=True,
        on_delete=models.SET_NULL)

    def __unicode__(self):
        if self.leader:
            return u"%s (%s %s)" % (
                self.title,
                self.leader.first_name,
                self.leader.last_name)
        else:
            return u"%s" % (self.title)

    def get_leader(self):
        if self.leader:
            return u"%s %s" % (
                self.leader.last_name,
                self.leader.first_name)

    get_leader.short_description = 'Староста'
