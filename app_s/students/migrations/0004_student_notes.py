# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('students', '0003_auto_20150529_1321'),
    ]

    operations = [
        migrations.AddField(
            model_name='student',
            name='notes',
            field=models.CharField(max_length=256, verbose_name='\u041d\u043e\u0442\u0430\u0442\u043a\u0438', blank=True),
            preserve_default=True,
        ),
    ]
