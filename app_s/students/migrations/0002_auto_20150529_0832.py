# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('students', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='group',
            name='title',
            field=models.CharField(unique=True, max_length=256, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='student',
            name='ticket',
            field=models.CharField(unique=True, max_length=256, verbose_name='\u0411\u0456\u043b\u0435\u0442'),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='student',
            unique_together=set([('first_name', 'middle_name', 'last_name')]),
        ),
    ]
